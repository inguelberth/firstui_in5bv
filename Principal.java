package org.inguelberthgarcia.sistema;

import javafx.application.Application;

import org.inguelberthgarcia.ui.App;

public class Principal{
	public static void main(String args[]){
		Application.launch(App.class, args);
	}
}
